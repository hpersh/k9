BIN	= k9

CC	= gcc -g

all:
	$(CC) -c -Wa,-ahlsm k9_amd64.s
	$(CC) -c -D__UNIT_TEST__ k9.c
	$(CC) -o $(BIN) -static k9.o k9_amd64.o

clean:
	rm -f $(BIN) *~ *.o
