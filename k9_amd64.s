	.bss

_intr_lvl:	.space	4
_intr_sp_save:	.space	8
_intr_stk_end:	.space	8
	
	.macro	push_all
	pushf
	pushq	%rax
	pushq	%rcx
	pushq	%rdx
	pushq	%rbx
	pushq	%rbp
	pushq	%rsi
	pushq	%rdi
	pushq	%r8
	pushq	%r9
	pushq	%r10
	pushq	%r11
	pushq	%r12
	pushq	%r13
	pushq	%r14
	pushq	%r15
	.endm

	.macro	pop_all
	popq	%r15
	popq	%r14
	popq	%r13
	popq	%r12
	popq	%r11
	popq	%r10
	popq	%r9
	popq	%r8
	popq	%rdi
	popq	%rsi
	popq	%rbp
	popq	%rbx
	popq	%rdx
	popq	%rcx
	popq	%rax
	popf
	.endm

	.text

	.global	k9_cpu_intr_stk_end_set
k9_cpu_intr_stk_end_set:
	movq	%rdi,_intr_stk_end
	ret
	
_intr_cntxt_leave:
	movq	_intr_sp_save,%rsp
	ret	
	
_intr_cntxt_enter:
	movq	%rsp,_intr_sp_save
	movq	_intr_stk_end,%rsp
	pushq	$_intr_cntxt_leave
	movq	%rdi,%rax
	movq	%rsi,%rdi
	jmp	*%rax

	.global	k9_cpu_isr
k9_cpu_isr:	
	movl	_intr_lvl,%eax
	incl	%eax
	movl	%eax,_intr_lvl
	cmpl	$1,%eax
	jne	k9_cpu_isr2
	call	_intr_cntxt_enter
	jmp	k9_cpu_isr3
k9_cpu_isr2:
	movq	%rdi,%rax
	movq	%rsi,%rdi
	call	*%rax
k9_cpu_isr3:
#	cli			# In case called ISR re-enabled interrupts
	decl	_intr_lvl
	call	_k9_task_resched
	ret

	.global	k9_cpu_intr_lvl
k9_cpu_intr_lvl:	
	movl	_intr_lvl,%eax
	ret
	
_task_exit:
	pushq	$0
	call	k9_task_exit

	.global	k9_cpu_cntxt_init
k9_cpu_cntxt_init:
	movq	%rdi,%rax	# rax = task sp
	lea	((16+1+1)*-8)(%rax),%rax	# Back up rax, 16 regs to pop + entry addr + top-level return addr
	movq	%rsi,(16*8)(%rax)	# Write entry point in context
	movq	$_task_exit,((16+1)*8)(%rax) # Write top level return addr
	movq	%rdx,(8*8)(%rax)	# Write entry arg
	ret
	
	.global	k9_cpu_cntxt_save
k9_cpu_cntxt_save:
	push_all
	movq	%rsp,(%rdi)
	movq	(16*8)(%rsp),%rax	# rax = return addr
	jmp	*%rax

	.global	k9_cpu_cntxt_restore
k9_cpu_cntxt_restore:
	movq	%rdi,%rsp
	pop_all
	xorq	%rax,%rax
	ret

	.global k9_cpu_intr_dis
k9_cpu_intr_dis:
	pushf
#	cli
	popq	%rax
	ret

	.global k9_cpu_intr_restore
k9_cpu_intr_restore:
#	movl	4(%rsp),%rax
#	pushl	%rax
#	popf
	ret

	.global	k9_cpu_wait
k9_cpu_wait:
#	hlt
	ret
