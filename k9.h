#include <assert.h>

#include "k9_cfg.h"

typedef unsigned char  uint8;
typedef unsigned short uint16;
typedef unsigned int   uint32;
typedef signed char    int8;
typedef signed short   int16;
typedef signed int     int32;

struct list {
  struct list *prev, *next;
};

#define LIST_INIT(li)   ((li)->prev = (li)->next = (li))
#define LIST_FIRST(li)  ((li)->next)
#define LIST_LAST(li)   ((li)->prev)
#define LIST_END(li)    (li)
#define LIST_PREV(p)    ((p)->prev)
#define LIST_NEXT(p)    ((p)->next)

static struct list *
list_insert(struct list * const nd, struct list * const before)
{
  struct list *p = before->prev;

  nd->prev = p;
  nd->next = before;

  return (p->next = before->prev = nd);
}

static struct list *
list_erase(struct list * const nd)
{
  struct list *p = nd->prev, *q = nd->next;

  (p->next = q)->prev = p;

  nd->prev = nd->next = 0;

  return (nd);
}

struct pq_node {
  struct pq *pq;
  unsigned  idx;
};


enum {
  K9_OK        = 0,
  K9_TIMED_OUT = -1
};

enum {
  K9_OBJ_MAGIC_TASK  = 0x49395441, /* "K9TA" */
  K9_OBJ_MAGIC_EV    = 0x49394556, /* "K9EV" */
  K9_OBJ_MAGIC_MUTEX = 0x49394d55, /* "K9MU" */
  K9_OBJ_MAGIC_SEM   = 0x49395345  /* "K9SE" */
};

struct k9_obj {
  uint32        magic;
  struct k9_obj *parent;
  char          *id;
};

enum {
  K9_EV_FLAG_INTR_SAFE   = 1 << 0,
  K9_EV_FLAG_PRI_INHERIT = 1 << 1
};

struct k9_ev {
  struct k9_obj base[1];

  unsigned    flags;
  struct list list[1];
};
typedef struct k9_ev *k9_ev_t;

struct k9_ev_wait_desc {
  k9_ev_t ev;
    
  unsigned       flag;
  struct list    list_node[1];
  struct k9_task *task;
};
typedef struct k9_ev_wait_desc *k9_ev_wait_desc_t;

enum {
  K9_TMOUT_NONE    = 0,
  K9_TMOUT_FOREVER = (unsigned) -1
};

struct k9_ev *k9_ev_init(k9_ev_t ev, char *id);
int          k9_ev_wait(unsigned nwait, k9_ev_wait_desc_t wait, unsigned tmout);
int          k9_ev_wait1(k9_ev_t ev, unsigned tmout);
void         k9_ev_signal(k9_ev_t ev);
void         k9_ev_signal_all(k9_ev_t ev);

enum {
  K9_TASK_FLAG_SUSPENDED  = 1 << 0,
  K9_TASK_FLAG_NO_PREEMPT = 1 << 1
};

enum {
  K9_TASK_STATE_NEW,
  K9_TASK_STATE_READY,
  K9_TASK_STATE_RUNNING,
  K9_TASK_STATE_BLOCKED,
  K9_TASK_STATE_STOPPED,
  K9_TASK_STATE_EXITED,
  K9_TASK_STATE_FAULTED
};

struct k9_task {
  struct k9_obj base[1];

  uint8  flags, iflags;
  uint16 slice;
  int16  pri, effpri;
  void   *data;

  struct list list_node[1];

  uint8  state;
  int    exit_code;
  uint16 cur_slice;
  void   *sp;

  struct {
    struct {
      unsigned blocked;
      unsigned ready;
      unsigned running;
    } ticks[1];
  } stats[1];

    struct {
      struct {
	unsigned          nwait;
	k9_ev_wait_desc_t wait;
      } ev[1];
      struct {
	struct pq_node pq_node[1];
	unsigned       deadline;
      } tmout[1];
      struct {
	int n;
      } sem[1];
      int rc;
    } blocked[1];
    struct {
      struct pq_node pq_node[1];
    } ready[1];
};
typedef struct k9_task *k9_task_t;

struct k9_task *k9_task_init(k9_task_t task, char *id, void *sp, void (*entry)(void *), void *arg);
struct k9_task *k9_task_start(k9_task_t task);
void           k9_task_sleep(unsigned tmout);
void           k9_task_yield(void);
void           k9_task_exit(int code);
unsigned       k9_task_flags_set(k9_task_t task, unsigned val, unsigned mask);
int            k9_task_pri_set(k9_task_t task, int pri);
struct k9_task *k9_task_self(void);
void           k9_tick(void);
void           k9_isr(void (*func)(void *), void *arg);

struct k9_mutex {
  struct k9_ev   base[1];
  struct k9_task *owner;
};
typedef struct k9_mutex *k9_mutex_t;

void k9_mutex_init(k9_mutex_t m, char *id);
int  k9_mutex_take(k9_mutex_t m, unsigned tmout);
void k9_mutex_give(k9_mutex_t m);

struct k9_sem {
  struct k9_ev base[1];
  int          cnt;
};
typedef struct k9_sem *k9_sem_t;

void k9_sem_init(k9_sem_t s, char *id, int cnt);
int  k9_sem_take(k9_sem_t s, unsigned n, unsigned tmout);
void k9_sem_give(k9_sem_t s, unsigned n);

void k9_init(void);
void k9_start(k9_task_t root_task, k9_task_t idle_task, void *intr_stk_end);


struct k9_mesg {
    struct list list_node[1];
    unsigned char data[64];
};

struct k9_mesgq {
    struct k9_obj   base[1];
    struct k9_mutex mutex[1];
    struct list     queue[1];
    
};
